package youdao.test.study;

public class ArgsCalculator {
    int sum(int a, int b) {
        return a + b;
    }

    int subtraction(int a, int b) {
        return a - b;
    }

    int multiplication(int a, int b) {
        return a * b;
    }

    int divison(int a, int b) throws Exception {
        if (b == 0) {
            throw new Exception("Divider can't be zero");
        }

        return a / b;
    }

    boolean equalIntegers(int a, int b) {
        boolean result = false;

        if (a == b) {
            result = true;
        }

        return result;
    }
}
